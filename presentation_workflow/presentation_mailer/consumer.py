import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()

while True:
    try:
        def process_message(ch, method, properties, body):
            info = json.loads(body)
            name = info["presenter_name"]
            title = info["title"]
            email = info["presenter_email"]
            send_mail(
                'Your presentation has been accepted',
                f"{name}, we're happy to tell you that your presentation{title}has been accepted",
                'admin@conference.go',
                [email],
                fail_silently=False,
            )

        def process_rejection(ch, method, properties, body):
            info = json.loads(body)
            name = info["presenter_name"]
            title = info["title"]
            email = info["presenter_email"]

            send_mail(
                'Your presentation has been rejected',
                f"{name}, your presentation{title}has been rejected",
                'admin@conference.go',
                [email],
                fail_silently=False,
            )
        parameters = pika.ConnectionParameters(host='rabbitmq')
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()
        channel.queue_declare(queue='presentation_approvals')
        channel.queue_declare(queue="presentation_rejections")
        channel.basic_consume(
            queue='presentation_approvals',
            on_message_callback=process_message,
            auto_ack=True,
        )

        channel.basic_consume(
            queue="presentation_rejections",
            on_message_callback=process_rejection,
            auto_ack=True,
        )
        channel.start_consuming()
    except AMQPConnectionError:
        print("could not connect to RabbitMQ")
        time.sleep(2.0)



# def process_message(ch, method, properties, body):
#     subject = "Your done with your presentation"
#     message = f"{body.presenter_name}, we're happy to tell you that your presentation{body.title}has been accepted"
#     email_from='harryzeng1998@gmail.com'
#     recipient_list=[body.presenter_email]
#     send_mail(subject,message, email_from, recipient_list)


# parameters = pika.ConnectionParameters(host='rabbitmq')
# connection = pika.BlockingConnection(parameters)
# channel = connection.channel()
# channel.queue_declare(queue='presentation_approvals')
# channel.basic_consume(
#     queue='presentation_approvals',
#     on_message_callback=process_message,
#     auto_ack=True,
# )
# channel.start_consuming()
